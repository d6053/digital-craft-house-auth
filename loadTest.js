import http from "k6/http";
import { check } from "k6";

export const options = {
  stages: [
    { target: 200, duration: "30s" },
    { target: 0, duration: "30s" },
  ],
};

export default function () {
  let data = `{
    "Email":"email@email.com",
    "Password":"510HdYx5*QR1!aTV$V5lYbDh8_BzQb9fxNFKvmSlTG6wNFNl6cDgAcwCnJ8W7zVU4iJuhHk_iHzR6_hF6EvkEloKdXX5i8FaQTex3lA==",
    "RememberMe":false
}`;

  const result = http.post("http://localhost:50198/auth/login", data);
  check(result, {
    "http response status code is 201": result.status === 201,
  });
}
