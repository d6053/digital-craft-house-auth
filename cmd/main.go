package main

import (
	"context"
	config "digital-craft-house-auth/src/configuration"
	"digital-craft-house-auth/src/grpc/protogo/login"
	"digital-craft-house-auth/src/http/rest"
	redisStorage "digital-craft-house-auth/src/storage/redis"

	"fmt"
	"log"
)

func main() {

	ctx := context.Background()
	//Load variables from .env file
	config, err := config.LoadEnv()
	if err != nil {
		log.Fatalf(err.Error())
	}

	redisDB, err := redisStorage.InitializeConnection(config.RedisDB.Url)
	if err != nil {
		fmt.Println(err.Error())
	}

	addr := fmt.Sprintf("%s:%s", config.Grpc.ClientHost, config.Grpc.ClientPort)
	loginGrpcClient, err := login.NewLoginGrpcClient(addr, config.Grpc.AuthKey)
	if err != nil {
		log.Fatalf(err.Error())
	}

	svr := rest.NewServer(config.Http.Port)
	svr.Init(ctx, loginGrpcClient, config.Grpc.ServerHost, config.Grpc.ServerPort, redisDB, config.RabbitMQ.Url)

	svr.Run(config.Http.AllowedOrigins)
}
