# Digital Craft House

## Context

Digital Craft House is a user-to-user advertising website where artists, crafters, or other people that create handmade items can sell their stock in their digital shops. There are different types of users, as the sellers can create products and the sellers can buy them. Think about the project as a smaller copy of Marketplaats which is limited in the specific field.

## Architecture

The project main focus is on microservices as there are three microservices (User, Auth, and Product).
When it comes to synchronous communication they are using gRPC between themselves. Each of them has its own database as the User and the Product microservice are using SQL relational database for storing users and products details, and the Auth Service is using Redis for storing access and refresh tokens.
On the other hand, when it comes to asynchronous communication between the User and the Auth service, they are using Message Broker (RabbitMQ).
When the React application wants to send a request to the backend, it firstly goes through the Gateway which in this case is a Kong gateway, and then the request is redirected to the responsible for the request service.

<img src="./images/c3.png" />

## Overview

- 3 **microservices** (User, Product, Auth). Each has separate repository located in the Digital Craft House group
- **MySQL** database (User and Product microservices)
- **RedisDB** (for storing access and refresh JWTs in Auth service)
- Authentication
  <img src="./images/get-seller-flow.png" />

- Synchronous communication via **gRPC**
- Asynchronous communication via **RabbitMQ**
- Unit tests (present only in Product service)
- **Kong** gateway for single entry point to the ecosystem
- Load testing via **K6**
- Monitoring and visualization via **Prometheus** and **Grafana**
- Everything is orchestrated in **Kubernetes Cluster**
- Frontend (React)

## DevOps

- CI/CD
  - Tests (present only in Product )
  - Build
  - SonarCloud analysis
  - Publish docker image in registry
  - CD (present only in User service)

<img src="./images/cicd.png" />
