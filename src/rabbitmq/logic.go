package rabbitmq

import (
	"bytes"
	"digital-craft-house-auth/src/domain"
	"encoding/json"
	"log"

	"github.com/streadway/amqp"
)

type rabbitmqService struct {
	// username string
	// password string
	// hostname string
	// port     string
	url string
}

func NewRabbitMQService(url string) RabbitMQService {
	return &rabbitmqService{

		// username,
		// password,
		// hostname,
		// port,
		url,
	}
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}

func (s *rabbitmqService) CreateUser(request domain.CreateSellerParams) {

	ch, err := EstablishConn(s.url)
	if err != nil {
		failOnError(err, "Failed to establish connection with rabbit mq")
	}
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"CreateUser", // name
		false,        // durable
		false,        // delete when unused
		false,        // exclusive
		false,        // no-wait
		nil,          // arguments
	)
	failOnError(err, "Failed to declare a queue")

	reqBodyBytes := new(bytes.Buffer)
	json.NewEncoder(reqBodyBytes).Encode(request)

	err = ch.Publish(
		"",     // exchange
		q.Name, // routing key
		false,  // mandatory
		false,  // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        reqBodyBytes.Bytes(),
		})
	failOnError(err, "Failed to publish a message")
}
