package rabbitmq

import "digital-craft-house-auth/src/domain"

type RabbitMQService interface {
	CreateUser(request domain.CreateSellerParams)
}
