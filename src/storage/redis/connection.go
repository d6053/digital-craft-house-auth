package redis

import (
	"github.com/go-redis/redis"
)

func InitializeConnection(redisUrl string) (*redis.Client, error) {
	addr, err := redis.ParseURL(redisUrl)
	if err != nil {
		panic(err)
	}
	client := redis.NewClient(addr)

	return client, nil
}
