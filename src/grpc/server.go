package grpc

import (
	auth "digital-craft-house-auth/src/auth"
	"digital-craft-house-auth/src/grpc/protogo/token"

	"fmt"
	"log"
	"net"

	"google.golang.org/grpc"
)

func GrpcServer(authService auth.AuthService, host string, port string) {
	lis, err := net.Listen("tcp", fmt.Sprintf("%s:%s", host, port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := token.Server{
		AuthService: authService,
	}

	grpcServer := grpc.NewServer()

	token.RegisterTokenServiceServer(grpcServer, &s)

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %s", err)
	}
}
