package token

import (
	"context"
	auth "digital-craft-house-auth/src/auth"
)

type Server struct {
	AuthService auth.AuthService
}

func (s *Server) ValidateToken(c context.Context, r *TokenRequest) (*TokenResponse, error) {
	tokenString := r.TokenString

	_, err := s.AuthService.VerifyToken(tokenString)
	if err != nil {
		response := TokenResponse{
			Valid: false,
			Error: "token is not valid",
		}
		return &response, nil
	}
	response := TokenResponse{
		Valid: true,
		Error: "",
	}
	return &response, nil
}
