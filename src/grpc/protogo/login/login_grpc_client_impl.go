package login

import (
	context "context"
	"log"

	grpc "google.golang.org/grpc"
)

type LoginGrpcClientImpl struct {
	grpcAuthKey string
	client      LoginServiceClient
}

func NewLoginGrpcClient(host, grpcAuthKey string) (*LoginGrpcClientImpl, error) {
	conn, err := grpc.Dial(host, grpc.WithInsecure())
	if err != nil {
		return nil, err
	}

	client := NewLoginServiceClient(conn)

	return &LoginGrpcClientImpl{
		grpcAuthKey: grpcAuthKey,
		client:      client,
	}, nil
}

func (c *LoginGrpcClientImpl) GetUserByCredentials(ctx context.Context, in *LoginRequest, opts ...grpc.CallOption) (*LoginResponse, error) {
	response, err := c.client.GetUserByCredentials(ctx, in)
	if err != nil {
		log.Fatalf("Error: %s", err)
	}
	return response, nil
}
