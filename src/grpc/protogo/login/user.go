package login

import "time"

type User struct {
	Id       int
	Name     string
	UserName string
	Password string
	Roles    []int
}

type TokenResponse struct {
	UUID        string    `json:"uuid"`
	Email       string    `json:"email"`
	FirstName   string    `json:"first_name"`
	LastName    string    `json:"last_name"`
	CreatedAt   time.Time `json:"created_at"`
	IsAdmin     bool      `json:"is_admin"`
	AccessToken string    `json:"access_token"`
}
