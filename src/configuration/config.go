package config

import (
	"os"

	"github.com/joho/godotenv"
)

type Config struct {
	Application Application
	Auth        Auth
	Grpc        Grpc
	Http        Http
	RabbitMQ    RabbitMQ
	RedisDB     RedisDB
}

func LoadEnv() (*Config, error) {
	env := os.Getenv("APP_ENV")
	if env != "production" && env != "develop" {
		err := godotenv.Load()
		if err != nil {
			return nil, err
		}
	}

	application := LoadApplicationEnv()
	auth := LoadAuthEnv()
	grpc := LoadGrpcEnv()
	http := LoadHTTPEnv()
	rabbitmq := LoadRabbitMQEnv()
	redis := LoadRedisDBEnv()

	conf := Config{
		Application: application,
		Auth:        auth,
		Grpc:        grpc,
		Http:        http,
		RabbitMQ:    rabbitmq,
		RedisDB:     redis,
	}

	return &conf, nil
}
