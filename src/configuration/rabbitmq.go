package config

import "os"

type RabbitMQ struct {
	Host     string
	Port     string
	Username string
	Password string
	Url      string
}

func LoadRabbitMQEnv() RabbitMQ {
	rabbit := RabbitMQ{
		Host:     os.Getenv("RABBIT_HOST"),
		Port:     os.Getenv("RABBIT_PORT"),
		Username: os.Getenv("RABBIT_USERNAME"),
		Password: os.Getenv("RABBIT_PASSWORD"),
		Url:      os.Getenv("RABBIT_URL"),
	}

	return rabbit
}
