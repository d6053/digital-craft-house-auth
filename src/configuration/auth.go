package config

import "os"

type Auth struct {
	JwtSecret string
}

func LoadAuthEnv() Auth {
	auth := Auth{
		JwtSecret: os.Getenv("JWT_SECRET"),
	}

	return auth
}
