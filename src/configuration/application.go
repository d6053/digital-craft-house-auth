package config

import "os"

type Application struct {
	AppName string
	Host    string
	Port    string
}

func LoadApplicationEnv() Application {
	app := Application{
		AppName: os.Getenv("APP_NAME"),
		Host:    os.Getenv("HOST"),
		Port:    os.Getenv("PORT"),
	}

	return app
}
