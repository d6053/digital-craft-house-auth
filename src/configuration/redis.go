package config

import "os"

type RedisDB struct {
	Host     string
	Port     string
	Password string
	Url      string
}

func LoadRedisDBEnv() RedisDB {
	redis := RedisDB{
		Host:     os.Getenv("REDIS_HOST"),
		Port:     os.Getenv("REDIS_PORT"),
		Password: os.Getenv("REDIS_PASSWORD"),
		Url:      os.Getenv("REDIS_URL"),
	}

	return redis
}
