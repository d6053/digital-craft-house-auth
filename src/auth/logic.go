package login

import (
	login "digital-craft-house-auth/src/grpc/protogo/login"
	"digital-craft-house-auth/src/token"

	"github.com/dgrijalva/jwt-go"
)

type authService struct {
}

func NewAuthService() AuthService {
	return &authService{}
}

func (s *authService) GetToken(user *login.LoginResponse) (*token.TokenDetails, error) {
	token, err := token.CreateToken(user.Uuid)
	if err != nil {
		return nil, err
	}
	return token, nil

}

func (s *authService) VerifyToken(tokenString string) (*jwt.Token, error) {
	token, err := token.VerifyToken(tokenString)
	if err != nil {
		return nil, err
	}
	return token, nil
}
