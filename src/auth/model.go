package login

type LoginRequest struct {
	Email      string `json:"Email" form:"Email" binding:"required"`
	Password   string `json:"Password" form:"Password" binding:"required"`
	RememberMe bool   `json:"RememberMe" form:"RememberMe"`
}
