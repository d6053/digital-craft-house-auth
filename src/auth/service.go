package login

import (
	auth "digital-craft-house-auth/src/grpc/protogo/login"
	"digital-craft-house-auth/src/token"

	"github.com/dgrijalva/jwt-go"
)

type AuthService interface {
	GetToken(user *auth.LoginResponse) (*token.TokenDetails, error)
	VerifyToken(tokenString string) (*jwt.Token, error)
}
