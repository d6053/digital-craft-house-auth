package token

import (
	"time"

	"github.com/go-redis/redis"
)

type redisRepository struct {
	client *redis.Client
}

// NewCustomer returns the customer service implementation.
func NewRedisRepository(client *redis.Client) RedisRepository {
	return &redisRepository{
		client,
	}
}

func (repository redisRepository) CreateAuth(userUuid string, td *TokenDetails) error {

	at := time.Unix(td.AtExpires, 0) //converting Unix to UTC(to Time object)
	// rt := time.Unix(td.RtExpires, 0)
	now := time.Now()

	errAccess := repository.client.Set(td.AccessUuid, userUuid, at.Sub(now)).Err()
	if errAccess != nil {
		return errAccess
	}
	// errRefresh := repository.client.Set(td.RefreshUuid, userUuid, rt.Sub(now)).Err()
	// if errRefresh != nil {
	// 	return errRefresh
	// }
	return nil
}

func (repository redisRepository) FetchAuth(authD *AccessDetails) (string, error) {
	userUuid, err := repository.client.Get(authD.AccessUuid).Result()
	if err != nil {
		return "", err
	}
	return userUuid, nil
}

func (repository redisRepository) DeleteAuth(givenUuid string) (int64, error) {
	deleted, err := repository.client.Del(givenUuid).Result()
	if err != nil {
		return 0, err
	}
	return deleted, nil
}
