package token

type RedisRepository interface {
	CreateAuth(userUuid string, td *TokenDetails) error
	FetchAuth(authD *AccessDetails) (string, error)
	DeleteAuth(givenUuid string) (int64, error)
}
