package token

import (
	"digital-craft-house-auth/src/util"
	"fmt"
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
)

const (
	jWTPrivateToken = "j65iZ4fsWEDKPqmZlZXaHhHSS1joQcBU"
)

func CreateToken(userUuid string) (*TokenDetails, error) {
	td := &TokenDetails{}
	td.AtExpires = time.Now().Add(time.Minute * 15).Unix()
	td.AccessUuid = uuid.New().String()

	td.RtExpires = time.Now().Add(time.Hour * 24 * 7).Unix()
	td.RefreshUuid = uuid.New().String()

	var err error
	//Creating Access Token
	os.Setenv("ACCESS_SECRET", "jdnfksdmfksd") //this should be in an env file
	atClaims := jwt.MapClaims{}
	atClaims["authorized"] = true
	atClaims["access_uuid"] = td.AccessUuid
	atClaims["user_uuid"] = userUuid
	atClaims["exp"] = td.AtExpires
	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	td.AccessToken, err = at.SignedString([]byte(os.Getenv("ACCESS_SECRET")))
	if err != nil {
		return nil, err
	}
	//Creating Refresh Token
	os.Setenv("REFRESH_SECRET", "mcmvmkmsdnfsdmfdsjf") //this should be in an env file
	rtClaims := jwt.MapClaims{}
	rtClaims["refresh_uuid"] = td.RefreshUuid
	rtClaims["user_uuid"] = userUuid
	rtClaims["exp"] = td.RtExpires
	rt := jwt.NewWithClaims(jwt.SigningMethodHS256, rtClaims)
	td.RefreshToken, err = rt.SignedString([]byte(os.Getenv("REFRESH_SECRET")))
	if err != nil {
		return nil, err
	}
	return td, nil
}

func VerifyToken(tokenString string) (*jwt.Token, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		//Make sure that the token method conform to "SigningMethodHMAC"
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(os.Getenv("ACCESS_SECRET")), nil
	})
	if err != nil {
		return nil, err
	}
	return token, nil
}

func TokenValid(tokenString string) error {
	token, err := VerifyToken(tokenString)
	if err != nil {
		return err
	}
	if _, ok := token.Claims.(jwt.Claims); !ok && !token.Valid {
		return err
	}
	return nil
}

func ExtractTokenMetadata(tokenString string) (*AccessDetails, error) {
	token, err := VerifyToken(tokenString)
	if err != nil {
		return nil, err
	}
	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {
		accessUuid, ok := claims["access_uuid"].(string)
		if !ok {
			return nil, util.WrapErrorf(nil, util.ErrorStatusInternal, util.ErrorCodeInternal, "access_uuid claim not valid")
		}
		userUuid, ok := claims["user_uuid"].(string)
		if !ok {
			return nil, util.WrapErrorf(nil, util.ErrorStatusInternal, util.ErrorCodeInternal, "user_uuid claim not valid")
		}
		return &AccessDetails{
			AccessUuid: accessUuid,
			UserUuid:   userUuid,
		}, nil
	}
	return nil, err
}

func RefreshToken(refreshToken RefreshTokenDetails, repository RedisRepository) (*Tokens, error) {

	//verify the token
	os.Setenv("REFRESH_SECRET", "mcmvmkmsdnfsdmfdsjf") //this should be in an env file
	token, err := jwt.Parse(refreshToken.RefreshToken, func(token *jwt.Token) (interface{}, error) {
		//Make sure that the token method conform to "SigningMethodHMAC"
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, util.WrapErrorf(nil, util.ErrorStatusInternal, util.ErrorCodeInternal, fmt.Sprintf("unexpected signing method: %v", token.Header["alg"]))
		}
		return []byte(os.Getenv("REFRESH_SECRET")), nil
	})
	//if there is an error, the token must have expired
	if err != nil {
		return nil, util.WrapErrorf(nil, util.ErrorStatusInternal, util.ErrorCodeInternal, "Refresh token expired")
	}
	//is token valid?
	if _, ok := token.Claims.(jwt.Claims); !ok && !token.Valid {
		return nil, util.WrapErrorf(nil, util.ErrorStatusUnauthorized, util.ErrorCodeUnauthorized, err.Error())

	}
	//Since token is valid, get the uuid:
	claims, ok := token.Claims.(jwt.MapClaims) //the token claims should conform to MapClaims
	if ok && token.Valid {
		refreshUuid, ok := claims["refresh_uuid"].(string) //convert the interface to string
		if !ok {
			return nil, util.WrapErrorf(nil, util.ErrorStatusInternal, util.ErrorCodeInternal, err.Error())
		}
		userUuid, ok := claims["user_uuid"].(string)
		if !ok {
			return nil, util.WrapErrorf(nil, util.ErrorStatusInternal, util.ErrorCodeInternal, err.Error())
		}
		//Delete the previous Refresh Token
		deleted, delErr := repository.DeleteAuth(refreshUuid)
		if delErr != nil || deleted == 0 { //if any goes wrong
			return nil, util.WrapErrorf(nil, util.ErrorStatusUnauthorized, util.ErrorCodeUnauthorized, delErr.Error())
		}
		//Create new pairs of refresh and access tokens
		ts, createErr := CreateToken(userUuid)
		if createErr != nil {
			return nil, util.WrapErrorf(nil, util.ErrorStatusInternal, util.ErrorCodeInternal, err.Error())
		}
		//save the tokens metadata to redis
		saveErr := repository.CreateAuth(userUuid, ts)
		if saveErr != nil {
			return nil, util.WrapErrorf(nil, util.ErrorStatusInternal, util.ErrorCodeInternal, err.Error())
		}
		tokens := Tokens{
			AccessToken:  ts.AccessToken,
			RefreshToken: ts.RefreshToken,
		}
		return &tokens, nil
	} else {
		return nil, util.WrapErrorf(nil, util.ErrorStatusUnauthorized, util.ErrorCodeUnauthorized, "refresh token had expired")
	}
}
