package rest

func (s *server) routes() {
	auth := s.Router.PathPrefix("/auth").Subrouter()

	auth.HandleFunc("/login", s.Login).Methods("POST")
	auth.HandleFunc("/register", s.RegisterSeller).Methods("POST")
	auth.HandleFunc("/logout", s.Logout).Methods("POST")

	auth.HandleFunc("/token/refresh", s.RefreshToken).Methods("POST")

}
