package rest

import (
	"context"
	auth "digital-craft-house-auth/src/auth"
	"digital-craft-house-auth/src/grpc"
	login "digital-craft-house-auth/src/grpc/protogo/login"
	"digital-craft-house-auth/src/rabbitmq"
	"digital-craft-house-auth/src/token"

	"digital-craft-house-auth/src/util"

	"log"
	"net/http"
	"time"

	"github.com/go-playground/validator"
	"github.com/go-redis/redis"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

type server struct {
	Server    *http.Server
	Router    *mux.Router
	Validator *validator.Validate

	AuthService     auth.AuthService
	UserGrpcClient  *login.LoginGrpcClientImpl
	RedisRepository token.RedisRepository
	RabbitMQService rabbitmq.RabbitMQService
}

func NewServer(port string) *server {
	address := ":" + port

	svr := &server{
		Server: &http.Server{
			Addr:         address,
			WriteTimeout: 15 * time.Second,
			ReadTimeout:  15 * time.Second,
		},
		Router: mux.NewRouter(),
	}

	svr.Router.NotFoundHandler = http.HandlerFunc(handleNotFound)

	svr.routes()

	return svr
}

func (s *server) Init(ctx context.Context, userGrpcClient *login.LoginGrpcClientImpl, grpcHost string, grpcServer string, client *redis.Client, rabbitUrl string) {
	s.Validator = validator.New()

	s.AuthService = auth.NewAuthService()
	s.UserGrpcClient = userGrpcClient
	s.RedisRepository = token.NewRedisRepository(client)
	s.RabbitMQService = rabbitmq.NewRabbitMQService(rabbitUrl)

	go grpc.GrpcServer(s.AuthService, grpcHost, grpcServer)
}

func (s *server) Run(allowedOrigins string) {

	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{http.MethodGet, http.MethodPost, http.MethodDelete, http.MethodPost},
		AllowedHeaders:   []string{"Content-Type", "Accept", "Access-Control-Allow-Credentials", "Authorization"},
		AllowCredentials: true,
	})

	s.Server.Handler = c.Handler(s.Router)

	if err := s.Server.ListenAndServe(); err != nil {
		log.Println(err)
	}
}

func handleNotFound(w http.ResponseWriter, r *http.Request) {
	err := util.NewErrorf(util.ErrorStatusNotFound, util.ErrorCodeNotFound, "Endpoint was not found.")
	renderErrorResponse(r.Context(), w, util.ErrorStatusNotFound, util.ErrorCodeNotFound, err.Error())
}
