package rest

import (
	"digital-craft-house-auth/src/domain"
	"digital-craft-house-auth/src/grpc/protogo/login"
	"digital-craft-house-auth/src/token"
	"digital-craft-house-auth/src/util"
	"time"

	"encoding/json"
	"net/http"

	"github.com/go-playground/validator"
)

func (s *server) Login(w http.ResponseWriter, r *http.Request) {

	var request login.LoginRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInvalid, util.ErrorCodeInvalid, err.Error())
		return
	}

	credentials := login.LoginRequest{
		Email:    request.Email,
		Password: request.Password,
	}

	user, err := s.UserGrpcClient.GetUserByCredentials(r.Context(), &credentials)
	if err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInvalid, util.ErrorCodeInvalid, err.Error())
	}

	ts, err := s.AuthService.GetToken(user)
	if err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInvalid, util.ErrorCodeInvalid, err.Error())
		return
	}

	err = s.RedisRepository.CreateAuth(user.Uuid, ts)
	if err != nil {
		renderErrorResponse(r.Context(), w, http.StatusUnprocessableEntity, util.ErrorCodeInternal, err.Error())
	}

	tokens := map[string]string{
		"email":         user.Email,
		"access_token":  ts.AccessToken,
		"refresh_token": ts.RefreshToken,
	}

	cookieAccessToken := &http.Cookie{
		Name:     "access_token",
		Value:    ts.AccessToken,
		Expires:  time.Now().Add(time.Hour * 8),
		HttpOnly: true,
		Path:     "/",
		SameSite: http.SameSiteNoneMode,
		Secure:   true,
	}
	http.SetCookie(w, cookieAccessToken)

	// cookieRefreshToken := &http.Cookie{
	// 	Name:     "refresh_token",
	// 	Value:    ts.RefreshToken,
	// 	Expires:  time.Now().Add(time.Hour * 8),
	// 	HttpOnly: true,
	// 	Path:     "/",
	// 	SameSite: http.SameSiteNoneMode,
	// 	Secure:   true,
	// }
	// http.SetCookie(w, cookieRefreshToken)

	renderResponse(w, http.StatusCreated, tokens)
}

func (s *server) RegisterSeller(w http.ResponseWriter, r *http.Request) {
	var request domain.CreateSellerParams

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInvalid, util.ErrorCodeInvalid, err.Error())
		return
	}

	defer r.Body.Close()

	if err := s.Validator.Struct(request); err != nil {
		errors := err.(validator.ValidationErrors)
		renderErrorResponse(r.Context(), w, util.ErrorStatusInvalid, util.ErrorCodeInvalid, errors.Error())
		return
	}

	s.RabbitMQService.CreateUser(request)

	renderResponse(w, http.StatusCreated, request)
}

func (s *server) Logout(w http.ResponseWriter, r *http.Request) {

	tokenAccessCookie, err := r.Cookie("access_token")
	if err != nil {
		renderErrorResponse(r.Context(), w, http.StatusInternalServerError, util.ErrorCodeInternal, err.Error())
	}

	auAccess, err := token.ExtractTokenMetadata(tokenAccessCookie.Value)
	if err != nil {
		renderErrorResponse(r.Context(), w, http.StatusUnprocessableEntity, util.ErrorCodeInternal, err.Error())
	}

	deleted, delErr := s.RedisRepository.DeleteAuth(auAccess.AccessUuid)
	if delErr != nil || deleted == 0 { //if any goes wrong
		renderErrorResponse(r.Context(), w, http.StatusUnauthorized, util.ErrorCodeUnauthorized, err.Error())
	}

	accessCookie := &http.Cookie{
		Name:     "access_token",
		Value:    "",
		Path:     "/",
		MaxAge:   -1,
		HttpOnly: true,
	}
	http.SetCookie(w, accessCookie)

	// tokenRefreshCookie, err := r.Cookie("refresh_token")
	// if err != nil {
	// 	renderErrorResponse(r.Context(), w, http.StatusInternalServerError, util.ErrorCodeInternal, err.Error())
	// }

	// auRefresh, err := token.ExtractTokenMetadata(tokenRefreshCookie.Value)
	// if err != nil {
	// 	renderErrorResponse(r.Context(), w, http.StatusUnprocessableEntity, util.ErrorCodeInternal, err.Error())
	// }

	// deletedRefresh, delRefreshErr := s.RedisRepository.DeleteAuth(auRefresh.AccessUuid)
	// if delRefreshErr != nil || deletedRefresh == 0 { //if any goes wrong

	// 	renderErrorResponse(r.Context(), w, http.StatusUnauthorized, util.ErrorCodeUnauthorized, err.Error())
	// }

	// refreshCookie := &http.Cookie{
	// 	Name:     "refresh_token",
	// 	Value:    "",
	// 	Path:     "/",
	// 	MaxAge:   -1,
	// 	HttpOnly: true,
	// }
	// http.SetCookie(w, refreshCookie)

	renderResponse(w, http.StatusOK, "Successfully logged out")
}

func (s *server) RefreshToken(w http.ResponseWriter, r *http.Request) {
	var request token.RefreshTokenDetails

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInvalid, util.ErrorCodeInvalid, err.Error())
		return
	}

	defer r.Body.Close()

	if err := s.Validator.Struct(request); err != nil {
		errors := err.(validator.ValidationErrors)
		renderErrorResponse(r.Context(), w, util.ErrorStatusInvalid, util.ErrorCodeInvalid, errors.Error())
		return
	}

	tokens, err := token.RefreshToken(request, s.RedisRepository)
	if err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInvalid, util.ErrorCodeInvalid, err.Error())
		return
	}
	renderResponse(w, http.StatusCreated, tokens)
}
